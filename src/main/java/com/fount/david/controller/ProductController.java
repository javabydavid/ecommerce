package com.fount.david.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fount.david.entity.Product;
import com.fount.david.exception.ProductNotFoundException;
import com.fount.david.service.IBrandService;
import com.fount.david.service.ICategoryService;
import com.fount.david.service.IProductService;

@Controller
@RequestMapping("/product")
public class ProductController {
	
	@Autowired
	private IProductService service;
	
	@Autowired
	private ICategoryService categoryService;
	
	@Autowired
	private IBrandService brandService;
	
	private void commonUi(Model model) {
		model.addAttribute("categories", categoryService.getCategoryIdAndName("ACTIVE"));
		model.addAttribute("brands", brandService.getBrandIdAndName());
	}
	
	//1. show Register page
	@GetMapping("/register")
	public String showRegister(Model model) {
		commonUi(model);
		return "ProductRegister";
	}
	
	//2. save product
	@PostMapping("/save")
	public String saveProduct(
			@ModelAttribute Product product,
			Model model
			) {
		try {
			Long id  = service.saveProduct(product);
			String message = "Product '"+id+"' created!";
			model.addAttribute("message", message);
		}catch(ProductNotFoundException e) {
			model.addAttribute("message", e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) {
			model.addAttribute("message", "Error! Unable to Save Product");
			e.printStackTrace();
		}
	
		commonUi(model);
		return "ProductRegister";
	}
	
	//3. list all products
	@GetMapping("/all")
	public String showAll(Model model,
						@RequestParam(value="message", required = false) String message) {
		
		try {
			List<Product> list = service.getAllProducts();
			model.addAttribute("list", list);
		} catch (ProductNotFoundException e) {
			model.addAttribute("message", e.getMessage());
		}catch(Exception e) {
			model.addAttribute("message", "Error! Unable to display Product list");
			e.printStackTrace();
		}
		return "ProductData";
	}
	
	@GetMapping("/delete")
	public String deleteProduct(@RequestParam Long id, 
								RedirectAttributes attributes) {
		try {
			service.deleteProduct(id);
			attributes.addAttribute("message", "Product Deleted Successful with id "+id);
		} catch (ProductNotFoundException e) {
		attributes.addAttribute("message", e.getMessage());
		}catch(Exception e) {
			attributes.addAttribute("message", "Error! Unable to Delete Product");
			e.printStackTrace();
		}
		return "redirect:all";
	}
	
	@GetMapping("/edit")
	public String editProduct(@RequestParam Long id,
							RedirectAttributes attributes,
							Model model) {
		String page =null;
		try {
		  Product product =service.getOneProduct(id);
			model.addAttribute("product", product);
			page ="ProductEdit";
		} catch(ProductNotFoundException e) {
			attributes.addAttribute("message", e.getMessage());
			page= "redirect:all";
		}catch (Exception e) {
			attributes.addAttribute("message", "Error! Product retrival fail");
			e.printStackTrace();
			page= "redirect:all";
		}
		commonUi(model);
		return page;
	}

	@PostMapping("/update")
	public String updateProduct(@ModelAttribute Product product,
							RedirectAttributes attributes) {
		try {
			service.updateProduct(product);
			attributes.addAttribute("message", "Product Updated with id "+product.getId());
		} catch(ProductNotFoundException e) {
			attributes.addAttribute("message", e.getMessage());
		}
		catch (Exception e) {
		 attributes.addAttribute("message", "Error! Unable to Update Product");
		 e.printStackTrace();
		}
		return "redirect:all";
	}
}
