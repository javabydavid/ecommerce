package com.fount.david.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fount.david.entity.User;
import com.fount.david.service.IUserService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUserService service;

	@GetMapping("/register")
	public String showReg() {
		return "UserRegister";
	}
	
	@PostMapping("/save")
	public String saveUser(
			@ModelAttribute User user,
			Model model
			) 
	{
		Long id = service.saveUser(user);
		model.addAttribute("message", "User created with id "+id);
		return "UserRegister";
	}
	
	
	@GetMapping("/all")
	public String showAllUser(Model model) {
		model.addAttribute("list", service.getAllUsers());
		return "UserData";
	}

}
