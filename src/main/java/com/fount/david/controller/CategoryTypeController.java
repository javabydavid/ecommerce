package com.fount.david.controller;

import com.fount.david.entity.CategoryType;
import com.fount.david.exception.CategoryTypeNotFoundException;
import com.fount.david.service.ICategoryTypeService;
import java.lang.Long;
import java.lang.String;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/categorytype")
public class CategoryTypeController {
	@Autowired
	private ICategoryTypeService service;

	@GetMapping("/register")
	public String registerCategoryType(Model model) {
		return "CategoryTypeRegister";
	}

	@PostMapping("/save")
	public String saveCategoryType(@ModelAttribute CategoryType categorytype,
					Model model) {
		try {
			Long id=service.saveCategoryType(categorytype);
			model.addAttribute("message","CategoryType created with Id:"+id);
		} catch (CategoryTypeNotFoundException e) {
			Long id=service.saveCategoryType(categorytype);
			model.addAttribute("message","CategoryType fail to create with Id: "+id);
			e.printStackTrace();
		}
		
		return "CategoryTypeRegister";
	}

	@GetMapping("/all")
	public String getAllCategoryTypes(Model model,
			@RequestParam(value = "message", required = false) String message) {
		try {
			List<CategoryType> list=service.getAllCategoryTypes();
			model.addAttribute("list",list);
			model.addAttribute("message",message);
		} catch (CategoryTypeNotFoundException e) {
			model.addAttribute("message", e.getMessage());
			e.printStackTrace();
		}
		
		return "CategoryTypeData";
	}

	@GetMapping("/delete")
	public String deleteCategoryType(@RequestParam Long id, RedirectAttributes attributes) {
		try {
			service.deleteCategoryType(id);
			attributes.addAttribute("message","CategoryType deleted with Id: "+id);
		} catch(CategoryTypeNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editCategoryType(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page=null;
		try {
			CategoryType ob=service.getOneCategoryType(id);
			model.addAttribute("categorytype",ob);
			page="CategoryTypeEdit";
		} catch(CategoryTypeNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateCategoryType(@ModelAttribute CategoryType categorytype,
			RedirectAttributes attributes) {
		
		try {
			service.updateCategoryType(categorytype);
			attributes.addAttribute("message","CategoryType updated");
		} catch (CategoryTypeNotFoundException e) {
			attributes.addAttribute("message", e.getMessage());
			e.printStackTrace();;
			
		}
		return "redirect:all";
	}
}
