package com.fount.david.controller;

import com.fount.david.entity.Brand;
import com.fount.david.exception.BrandNotFoundException;
import com.fount.david.service.IBrandService;
import java.lang.Long;
import java.lang.String;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/brand")
public class BrandController {
	
	@Autowired
	private IBrandService service;

	@GetMapping("/register")
	public String registerBrand(Model model) {
		return "BrandRegister";
	}

	@PostMapping("/save")
	public String saveBrand(@ModelAttribute Brand brand,
							Model model) {
		
		try {
			Long id=service.saveBrand(brand);
			model.addAttribute("message","Brand created with Id:"+id);
		} catch (BrandNotFoundException e) {
			model.addAttribute("message", "Unable to Save Brand");
			e.printStackTrace();
		}catch (Exception e) {
			model.addAttribute("message", "Error occur during Brand save");
			e.printStackTrace();
		}
		return "redirect:register";
	}

	@GetMapping("/all")
	public String getAllBrands(Model model,
			@RequestParam(value = "message", 
			required = false) String message) {
		List<Brand> list=service.getAllBrands();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "BrandData";
	}

	@GetMapping("/delete")
	public String deleteBrand(@RequestParam Long id, 
							RedirectAttributes attributes) {
		
		try {
			service.deleteBrand(id);
			attributes.addAttribute("message","Brand deleted with Id:"+id);
		} catch(BrandNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
		}
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editBrand(@RequestParam Long id, 
								Model model, 
								RedirectAttributes attributes) {
		String page=null;
		try {
			Brand ob=service.getOneBrand(id);
			model.addAttribute("brand",ob);
			page="BrandEdit";
		} catch(BrandNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
			page="redirect:all";
		}
		return page;
	}

	@PostMapping("/update")
	public String updateBrand(@ModelAttribute Brand brand,
			RedirectAttributes attributes) {
	
		try {
			service.updateBrand(brand);
			attributes.addAttribute("message","Brand updated");
		} catch (BrandNotFoundException e) {
		attributes.addAttribute("message", "Unable to update Brand Record");
		}
	
		return "redirect:all";
	}
}
