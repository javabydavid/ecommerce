package com.fount.david.controller;

import com.fount.david.entity.Category;
import com.fount.david.exception.CategoryNotFoundException;
import com.fount.david.service.ICategoryService;
import com.fount.david.service.ICategoryTypeService;

import lombok.extern.slf4j.Slf4j;

import java.lang.Long;
import java.lang.String;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/category")
@Slf4j
public class CategoryController {
	@Autowired
	private ICategoryService service;

	@Autowired
	private ICategoryTypeService categoryService;
	
	
	
	public void commonUi(Model model) {
		model.addAttribute("categoryTypes", categoryService.getCategoryTypeIdAndName());
	}
	
	
	@GetMapping("/register")
	public String registerCategory(Model model) {
		commonUi(model);

		return "CategoryRegister";
	}

	@PostMapping("/save")
	public String saveCategory(@ModelAttribute Category category, Model model) {
		try {
			Long id=service.saveCategory(category);
			model.addAttribute("message","Category created with Id: "+id);
		} catch (CategoryNotFoundException e) {
			model.addAttribute("message", e.getMessage());
		}
		commonUi(model);
		return "CategoryRegister";
	}

	@GetMapping("/all")
	public String getAllCategorys(Model model,
			@RequestParam(value = "message", required = false) String message) {
		List<Category> list=service.getAllCategorys();
		model.addAttribute("list",list);
		model.addAttribute("message",message);
		return "CategoryData";
	}

	@GetMapping("/delete")
	public String deleteCategory(@RequestParam Long id, RedirectAttributes attributes) {
		
		log.info("ENTERED INTO DELETE METHOD");
		try {
			service.deleteCategory(id);
			attributes.addAttribute("message","Category deleted with Id:"+id);
			
			log.debug("CATEGORY DELETED WITH ID {} ", +id);
		} catch(CategoryNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
			log.error("ERROR IS : {} ", id);
		}
		log.info("ABOUT TO LEAVE DELETE METHOD");
		return "redirect:all";
	}

	@GetMapping("/edit")
	public String editCategory(@RequestParam Long id, Model model, RedirectAttributes attributes) {
		String page=null;
		try {
			Category ob=service.getOneCategory(id);
			model.addAttribute("category",ob);
			commonUi(model);
			page="CategoryEdit";
		} catch(CategoryNotFoundException e) {
			e.printStackTrace() ;
			attributes.addAttribute("message",e.getMessage());
			page="redirect:all";
		}	
		commonUi(model);
		return page;
	}

	@PostMapping("/update")
	public String updateCategory(@ModelAttribute Category category, RedirectAttributes attributes) {
		try {
			service.updateCategory(category);
			attributes.addAttribute("message","Category updated");
		} catch(CategoryNotFoundException e) {
			attributes.addAttribute("message", e.getMessage());
		}
		catch (Exception e) {
		attributes.addAttribute("message", "Error! Unable to Update Category");
		e.printStackTrace();
		}
		
		return "redirect:all";
	}
}
