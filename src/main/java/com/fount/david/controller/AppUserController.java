package com.fount.david.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fount.david.entity.AppUser;
import com.fount.david.exception.AppUserNotFoundException;
import com.fount.david.service.impl.AppUserServiceImpl;

@Controller
@RequestMapping("/appuser")
public class AppUserController {

	@Autowired
	private AppUserServiceImpl service;
	
	//1. show Register page
		@GetMapping("/register")
		public String showReg() {
			return "AppUserRegister";
		}
		
		//2. save User
		@PostMapping("/save")
		public String saveUser(
				@ModelAttribute AppUser appUser,
				Model model){
			try {
				Long id = service.saveAppUser(appUser);
				model.addAttribute("message", "User '"+id+"' is created");
			} catch (AppUserNotFoundException e) {
				e.printStackTrace();
			}
	;
			return "AppUserRegister";
		}
		//3. show data
		@GetMapping("/all")
		public String showData(Model model) {
			model.addAttribute("list", service.getAllAppUser());
			return "AppUserData";
		}

}
