package com.fount.david.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fount.david.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Long> {

}
