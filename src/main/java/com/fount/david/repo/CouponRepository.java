package com.fount.david.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fount.david.entity.Coupon;

public interface CouponRepository extends JpaRepository<Coupon, Integer> {
}
