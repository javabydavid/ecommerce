package com.fount.david.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.fount.david.entity.Brand;

public interface BrandRepository extends JpaRepository<Brand, Long> {

	@Query("SELECT id, name FROM Brand")
	List<Object[]> getBrandIdAndName();

}
