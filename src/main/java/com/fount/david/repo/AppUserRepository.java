package com.fount.david.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fount.david.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

}
