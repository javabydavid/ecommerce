package com.fount.david.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fount.david.entity.Shipping;

public interface ShippingRepository extends JpaRepository<Shipping, Long> {
		

}
