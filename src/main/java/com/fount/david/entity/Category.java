package com.fount.david.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "category_tab")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name="category_name")
	private String name;
	
	@Column(name="category_alias")
	private String alias;
	
	@Column(name="category_note")
	private String note;
	
	@Column(name="category_status")
	private String status;
	
	//---Integrations-----
	
	@ManyToOne
	@JoinColumn(name="category_type_fk")
	private CategoryType categoryType;
}
