package com.fount.david.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fount.david.constants.UserRole;

import lombok.Data;

@Data
@Entity
@Table(name="user_tbl")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="usr_id")
	private Long id;
	
	@Column(
			name="usr_name",
			length = 45,
			nullable = false
			)
	private String displayName;
	
	@Column(
			name="usr_email",
			length = 75,
			nullable = false,
			unique = true
			)
	private String email;
	
	@Column(
			name="usr_pwd",
			length = 120,
			nullable = false,
			unique = true
			)
	private String password;
	
	@Column(
			name="usr_status",
			nullable = false
			)
	private String status;
	
	@Column(name="usr_contact",
			nullable = false
			)
	private String contact;
	
	@Column(name="usr_role",
			nullable = false
			)
	@Enumerated(EnumType.STRING)
	private UserRole role;
	
	@Column(name="user_addr",
			nullable = false
			)
	private String address;

	
}
