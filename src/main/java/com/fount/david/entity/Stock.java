package com.fount.david.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Data;

@Data
@Entity
@Table(name="stock_tab")
public class Stock {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="stock_qoh")
	private Long qoh;
	
	@Column(name="stock_sold")
	private Long sold;
	
	@OneToOne
	@JoinColumn(name="prod_fk")
	private Product product;
	
	@Transient
	private Long count;

}
