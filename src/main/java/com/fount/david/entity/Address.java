package com.fount.david.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name="addr_tbl")
public class Address {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="addr_id")
	private Long id;

	@Column(name="addr_line1")
	private String line1;
	
	@Column(name="addr_line2")
	private String line2;
	
	@Column(name="addr_city")
	private String city;
	
	@Column(name="addr_state")
	private String state;
	
	@Column(name="addr_country")
	private String country;
	
	@Column(name="addr_pincode")
	private String pincode;

	@Override
	public String toString() {
		return line1+", "+line2+", "+city+", "+state+", "+country+", "+pincode;
	}

	
}
