package com.fount.david.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "shipping_tab")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Shipping {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name="ship_type")
	private String shipType;
	
	@Column(name="ship_code")
	private String shipCode;
	
	@Column(name="ship_name")
	private String shipName;
	
	@Column(name="ship_cost")
	private String shipCost;
	
	@Column(name="ship_weight")
	private String shipWeight;
	
	@Column(name="ship_weight_type")
	private String shipWeightType;
	
	@Column(name="ship_note")
	private String note;

}
