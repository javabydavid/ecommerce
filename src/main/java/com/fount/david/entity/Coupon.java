package com.fount.david.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "coupon_tab")
public class Coupon {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name="coupon_code")
	private String code;
	
	@Column(name="coupon_note")
	private String note;
	
	@Column(name="coupon_percentage")
	private int percentage;
	
	@Column(name="coupon_available")
	private String available;
	
	@DateTimeFormat(iso = ISO.DATE)
	@Temporal(TemporalType.DATE)
	@Column(name="coupon_exp_date")
	private Date date;
	
	
	@Column(name="coupon_totalAllowed")
	private String totalAllowed;
	
	@Column(name="coupon_claimed_count")
	private Long claimedCount;
}
