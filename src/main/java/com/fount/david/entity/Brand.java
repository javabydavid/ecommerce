package com.fount.david.entity;

import java.lang.Long;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "brand_tab")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Brand {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name="brand_name")
	private String name;
	
	@Column(name="brand_code")
	private String code;
	
	@Column(name="brand_tagline")
	private String tagline;
	
	@Column(name="brand_image_link")
	private String imageLink;
	
	@Column(name="brand_note")
	private String note;
	
	
}
