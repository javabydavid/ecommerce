package com.fount.david.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="product_tab")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="product_id")
	private Long id;
	
	@Column(name="product_name")
	private String name;

	@Column(name="product_short_desc")
	private String shortDesc;
	
	@Column(name="product_full_desc")
	private String fullDesc;
	
	@Column(name="product_status")
	private String status;
	
	@Column(name="product_stock")
	private String inStock;
	
	@Column(name="product_cost")
	private Double cost;
	
	@Column(name="product_length")
	private Double len;
	
	@Column(name="product_width")
	private Double wid;
	
	@Column(name="product_height")
	private Double height;
	
	@Column(name="product_dim_type")
	private String dimType;
	
	@Column(name="product_weight")
	private Double weight;
	
	@Column(name="product_weight_type")
	private String wigType;
	
	@Column(name="product_note")
	private String note;
	
	//---Module Integration
	
	@ManyToOne
	@JoinColumn(name="catygory_fk")
	private Category category;
	
	@ManyToOne
	@JoinColumn(name="brand_fk")
	private Brand brand;

}
