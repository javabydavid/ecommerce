package com.fount.david.exception;

import java.lang.RuntimeException;
import java.lang.String;

public class CategoryNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

public CategoryNotFoundException() {
  }

  public CategoryNotFoundException(String message) {
    super(message);
  }
}
