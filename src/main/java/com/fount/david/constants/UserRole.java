package com.fount.david.constants;

public enum UserRole {
	ADMIN, SALES, EMPLOYEE, CUSTOMER
}
