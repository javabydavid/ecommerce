package com.fount.david.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fount.david.entity.Coupon;
import com.fount.david.repo.CouponRepository;
import com.fount.david.service.ICouponService;


@Service
public class CouponServiceImpl implements ICouponService {
  @Autowired
  private CouponRepository repo;

  @Override
  @Transactional
  public Integer saveCoupon(Coupon coupon) {
    return repo.save(coupon).getId();
  }

  @Override
  @Transactional
  public void updateCoupon(Coupon coupon) {
    repo.save(coupon);
  }

  @Override
  @Transactional
  public void deleteCoupon(Integer id) {
    repo.deleteById(id);
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public Coupon getOneCoupon(Integer id) {
    return repo.findById(id).get();
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public List<Coupon> getAllCoupons() {
    return repo.findAll();
  }
}
