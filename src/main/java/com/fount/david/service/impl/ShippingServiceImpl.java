package com.fount.david.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fount.david.entity.Shipping;
import com.fount.david.exception.ShippingNotFoundException;
import com.fount.david.repo.ShippingRepository;
import com.fount.david.service.IShippingService;

@Service
public class ShippingServiceImpl implements IShippingService {

	@Autowired
	private ShippingRepository repo;
	
	public Long saveShipping(Shipping shipping) {
		
		return repo.save(shipping).getId();
	}

	
	public void updateShipping(Shipping shipping) {
	
		if(shipping.getId() == null || !repo.existsById(shipping.getId())) 
			
			throw new ShippingNotFoundException("Shipping Not Exist");
		else
			repo.save(shipping);
		
	}

	
	public void deleteShipping(Long id) {
		repo.delete(getOneShipping(id));
		
	}
	
	public Shipping getOneShipping(Long id) {
	
		return repo.findById(id).orElseThrow(
				()-> new ShippingNotFoundException("Shipping Not Found"));
	}

	
	public List<Shipping> getAllShippings() {
		
		return repo.findAll();
	}

}
