package com.fount.david.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fount.david.entity.AppUser;
import com.fount.david.repo.AppUserRepository;
import com.fount.david.service.IAppUserService;

@Service
public class AppUserServiceImpl implements IAppUserService{

	@Autowired
	private AppUserRepository repo;
	
	@Override
	public Long saveAppUser(AppUser user) {
		
		return repo.save(user).getId();
	}

	@Override
	public List<AppUser> getAllAppUser() {
		
		return repo.findAll();
	}

}
