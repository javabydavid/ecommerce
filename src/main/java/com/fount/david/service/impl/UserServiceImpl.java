package com.fount.david.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fount.david.entity.User;
import com.fount.david.repo.UserRepository;
import com.fount.david.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository repo;

	
	public Long saveUser(User user) {
		
		return repo.save(user).getId();
	}

	
	public Optional<User> findByEmail(String email) {
		
		return repo.findByEmail(email);
	}

	
	public List<User> getAllUsers() {
		
		return repo.findAll();
	}
	

}
