package com.fount.david.service.impl;

import com.fount.david.entity.Brand;
import com.fount.david.repo.BrandRepository;
import com.fount.david.service.IBrandService;
import com.fount.david.util.AppUtil;

import java.lang.Long;
import java.lang.Override;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class BrandServiceImpl implements IBrandService {
  @Autowired
  private BrandRepository repo;

  @Override
  @Transactional
  public Long saveBrand(Brand brand) {
    return repo.save(brand).getId();
  }

  @Override
  @Transactional
  public void updateBrand(Brand brand) {
    repo.save(brand);
  }

  @Override
  @Transactional
  public void deleteBrand(Long id) {
    repo.deleteById(id);
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public Brand getOneBrand(Long id) {
    return repo.findById(id).get();
  }

  @Override
  @Transactional(
      readOnly = true
  )
  public List<Brand> getAllBrands() {
    return repo.findAll();
  }

@Override
public Map<Long, String> getBrandIdAndName() {
	List<Object[]> list = repo.getBrandIdAndName();
	return AppUtil.convertListToMapLong(list);
	
}
}
