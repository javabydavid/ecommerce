package com.fount.david.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fount.david.entity.Product;
import com.fount.david.exception.ProductNotFoundException;
import com.fount.david.repo.ProductRepository;
import com.fount.david.service.IProductService;
import com.fount.david.util.AppUtil;

@Service
public class ProductServiceImpl implements IProductService{

	@Autowired
	private ProductRepository repo;
	
	public Long saveProduct(Product product) {

		return repo.save(product).getId();
	}

	
	public void updateProduct(Product product) {
		
		if(product.getId() == null || !repo.existsById(product.getId())) 
			throw new ProductNotFoundException("Product Not Found");
		else 
			repo.save(product);	
	}
	
	public void deleteProduct(Long id) {
		
		repo.delete(getOneProduct(id));
	}

	
	public Product getOneProduct(Long id) {
	
		return repo.findById(id)
				.orElseThrow(
					()-> new ProductNotFoundException("Product No Found"));

	}

	
	public List<Product> getAllProducts() {
	
		return repo.findAll();
	}


	@Override
	public Map<Long, String> getProductIdAndName() {
		List<Object[]> list = repo.getProductIdAndNames();
		return AppUtil.convertListToMapLong(list);
	}

}
