package com.fount.david.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fount.david.entity.Address;
import com.fount.david.exception.AddressNotFoundException;
import com.fount.david.repo.AddressRepository;
import com.fount.david.service.IAddressService;
@Service
public class AddressServiceImpl implements IAddressService {

	@Autowired
	private AddressRepository repo;
	
	
	public Long saveAddress(Address address) {
		
		return repo.save(address).getId();
	}

	public List<Address> getAllAddress() {
		
		return repo.findAll();
	}


	public Address getOneAddress(Long id) {
		return repo.findById(id)
				.orElseThrow(
						() -> new AddressNotFoundException("Address not Found"));
	}

	
	public void deleteAddress(Long id) {	
		repo.delete(getOneAddress(id));

	}


	public void updateAddress(Address address) {	
		repo.save(address);

	}

}
