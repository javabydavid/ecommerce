package com.fount.david.service;

import com.fount.david.entity.Brand;
import java.lang.Long;
import java.util.List;
import java.util.Map;


public interface IBrandService {
	Long saveBrand(Brand brand);

	void updateBrand(Brand brand);

	void deleteBrand(Long id);

	Brand getOneBrand(Long id);

	List<Brand> getAllBrands();
	
	Map<Long, String> getBrandIdAndName();
}
