package com.fount.david.service;

import java.util.List;

import com.fount.david.entity.Address;

public interface IAddressService {

	public Long saveAddress(Address address);
	
	public List<Address> getAllAddress();
	
	public Address getOneAddress(Long id);
	
	public void deleteAddress(Long id);
	
	public void updateAddress(Address address);
}
