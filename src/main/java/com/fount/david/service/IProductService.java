package com.fount.david.service;

import java.util.List;
import java.util.Map;

import com.fount.david.entity.Product;

public interface IProductService {

	Long saveProduct(Product product);
	void updateProduct(Product product);
	void deleteProduct(Long id);
	Product getOneProduct(Long id);
	List<Product> getAllProducts();
	public Map<Long, String> getProductIdAndName();

}
