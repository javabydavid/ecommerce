package com.fount.david.service;

import java.util.List;

import com.fount.david.entity.Coupon;

public interface ICouponService {
	Integer saveCoupon(Coupon coupon);

	void updateCoupon(Coupon coupon);

	void deleteCoupon(Integer id);

	Coupon getOneCoupon(Integer id);

	List<Coupon> getAllCoupons();
}
