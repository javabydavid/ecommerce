package com.fount.david.service;

import java.util.List;
import java.util.Optional;

import com.fount.david.entity.User;

public interface IUserService {

	Long saveUser(User user);
	Optional<User> findByEmail(String email);
	List<User> getAllUsers();

}
