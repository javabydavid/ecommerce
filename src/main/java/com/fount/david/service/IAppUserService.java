package com.fount.david.service;

import java.util.List;

import com.fount.david.entity.AppUser;

public interface IAppUserService {
	
	Long saveAppUser(AppUser user);
	List<AppUser> getAllAppUser();

}
